UNRELEASED/UNFINISHED/UNDER DEVELOPMENT

Soot is a simple assembly language/instruction set architecture/virtual machine/bytecode. It is designed to be easy to implement. Its purpose is to make it easy to bootstrap Oot on a new platform; to bootstrap a (slow, limited) Oot, all you have to do is implement Soot, because Oot's virtual machine, the Ovm, (will be) implemented in Soot.

Soot is a RISC 3-operand fixed-length register machine with 16 instructions and 16 registers.


The instructions are:
0. annotate
1. loadi (load immediate)
2. load  (load from memory to register)
3. store (store from register to memory)
4. cpy   (copy from register to register (in other architectures this is usually called "MOV"))
5. skipz (skip if zero)
6. skipnz (skip if non-zero)
7. calli  (call indirect)
8. pop 
9. push
10. add-u16 (addition of u16s)
11. sub-u16 (subtraction of u16s)
12. leq-u16 (less-than-or-equal of u16s)
13. add-ptr (add a u16 to a pointer)
14. sub-ptr (subtract a u16 from a pointer)
15. syscall (call built-in system subroutine)

